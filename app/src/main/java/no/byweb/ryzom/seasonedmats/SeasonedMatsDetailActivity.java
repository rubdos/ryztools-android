package no.byweb.ryzom.seasonedmats;

import no.byweb.ryzom.dev.R;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

public class SeasonedMatsDetailActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_seasoned_mats_detail);

		getActionBar().setDisplayHomeAsUpEnabled(true);

		if (savedInstanceState == null) {
			Bundle arguments = new Bundle();
			arguments.putString(MaterialDetailFragment.ARG_ITEM_ID,
					getIntent().getStringExtra(MaterialDetailFragment.ARG_ITEM_ID));
			MaterialDetailFragment fragment = new MaterialDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction().add(R.id.material_detail_container, fragment).commit();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpTo(this, new Intent(this, SeasonedMatsActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
