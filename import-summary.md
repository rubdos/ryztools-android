ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* `.classpath.swp`
* `.gitignore`
* `.gitmodules`
* `README.md`
* `ic_launcher-web.png`
* `proguard-project.txt`
* `tools/`
* `tools/ryzomExtraToSQLite.php`
* `vendor/`
* `vendor/ryzom_extra/`
* `vendor/ryzom_extra/.git`
* `vendor/ryzom_extra/.gitignore`
* `vendor/ryzom_extra/LICENSE`
* `vendor/ryzom_extra/README.md`
* `vendor/ryzom_extra/bin/`
* `vendor/ryzom_extra/bin/config-sample.php`
* `vendor/ryzom_extra/bin/export.php`
* `vendor/ryzom_extra/composer.json`
* `vendor/ryzom_extra/lib/`
* `vendor/ryzom_extra/lib/RyzomExtra/`
* `vendor/ryzom_extra/lib/RyzomExtra/AtysDateTime.php`
* `vendor/ryzom_extra/lib/RyzomExtra/Cache.php`
* `vendor/ryzom_extra/lib/RyzomExtra/CacheInterface.php`
* `vendor/ryzom_extra/lib/RyzomExtra/Export/`
* `vendor/ryzom_extra/lib/RyzomExtra/Export/Application.php`
* `vendor/ryzom_extra/lib/RyzomExtra/Export/Encoder/`
* `vendor/ryzom_extra/lib/RyzomExtra/Export/Encoder/JsonEncoder.php`
* `vendor/ryzom_extra/lib/RyzomExtra/Export/Encoder/SerializeEncoder.php`
* `vendor/ryzom_extra/lib/RyzomExtra/Export/EncoderInterface.php`
* `vendor/ryzom_extra/lib/RyzomExtra/Export/ExportInterface.php`
* `vendor/ryzom_extra/lib/RyzomExtra/Export/PackedSheetsExport.php`
* `vendor/ryzom_extra/lib/RyzomExtra/Export/SheetIdExport.php`
* `vendor/ryzom_extra/lib/RyzomExtra/Export/Sheets/`
* `vendor/ryzom_extra/lib/RyzomExtra/Export/Sheets/AbstractSheetExport.php`
* `vendor/ryzom_extra/lib/RyzomExtra/Export/Sheets/ItemSheetExport.php`
* `vendor/ryzom_extra/lib/RyzomExtra/Export/Sheets/SbrickSheetExport.php`
* `vendor/ryzom_extra/lib/RyzomExtra/Export/Sheets/SkilltreeSheetExport.php`
* `vendor/ryzom_extra/lib/RyzomExtra/Export/VisualSlotExport.php`
* `vendor/ryzom_extra/lib/RyzomExtra/Export/WordsExport.php`
* `vendor/ryzom_extra/lib/RyzomExtra/GuildIconBuilder.php`
* `vendor/ryzom_extra/lib/RyzomExtra/GuildIconRenderer.php`
* `vendor/ryzom_extra/lib/RyzomExtra/RyzomClock.php`
* `vendor/ryzom_extra/phpunit.xml`
* `vendor/ryzom_extra/resources/`
* `vendor/ryzom_extra/resources/buildings.inc.php`
* `vendor/ryzom_extra/resources/guild-icon/`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_00_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_00_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_01_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_01_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_02_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_02_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_03_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_03_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_04_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_04_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_05_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_05_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_06_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_06_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_07_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_07_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_08_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_08_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_09_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_09_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_10_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_10_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_11_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_11_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_12_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_12_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_13_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_13_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_14_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_b_14_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_00_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_00_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_01_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_01_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_02_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_02_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_03_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_03_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_04_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_04_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_05_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_05_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_06_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_06_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_07_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_07_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_08_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_08_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_09_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_09_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_10_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_10_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_11_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_11_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_12_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_12_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_13_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_13_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_14_1.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_back_s_14_2.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_00.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_01.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_02.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_03.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_04.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_05.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_06.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_07.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_08.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_09.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_10.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_11.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_12.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_13.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_14.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_15.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_16.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_17.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_18.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_19.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_20.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_21.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_22.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_23.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_24.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_25.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_26.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_27.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_28.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_29.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_30.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_31.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_32.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_33.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_34.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_35.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_36.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_37.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_38.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_39.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_40.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_41.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_42.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_b_43.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_00.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_01.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_02.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_03.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_04.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_05.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_06.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_07.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_08.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_09.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_10.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_11.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_12.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_13.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_14.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_15.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_16.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_17.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_18.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_19.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_20.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_21.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_22.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_23.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_24.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_25.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_26.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_27.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_28.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_29.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_30.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_31.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_32.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_33.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_34.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_35.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_36.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_37.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_38.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_39.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_40.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_41.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_42.png`
* `vendor/ryzom_extra/resources/guild-icon/guild_symbol_s_43.png`
* `vendor/ryzom_extra/resources/sheets-cache/`
* `vendor/ryzom_extra/resources/sheets-cache/craftplan.serial`
* `vendor/ryzom_extra/resources/sheets-cache/items.serial`
* `vendor/ryzom_extra/resources/sheets-cache/resource_stats.serial`
* `vendor/ryzom_extra/resources/sheets-cache/sbrick.serial`
* `vendor/ryzom_extra/resources/sheets-cache/sheets-00.serial`
* `vendor/ryzom_extra/resources/sheets-cache/sheets-01.serial`
* `vendor/ryzom_extra/resources/sheets-cache/sheets-02.serial`
* `vendor/ryzom_extra/resources/sheets-cache/sheets-03.serial`
* `vendor/ryzom_extra/resources/sheets-cache/sheets-04.serial`
* `vendor/ryzom_extra/resources/sheets-cache/sheets-05.serial`
* `vendor/ryzom_extra/resources/sheets-cache/sheets-06.serial`
* `vendor/ryzom_extra/resources/sheets-cache/sheets-07.serial`
* `vendor/ryzom_extra/resources/sheets-cache/sheets-08.serial`
* `vendor/ryzom_extra/resources/sheets-cache/sheets-09.serial`
* `vendor/ryzom_extra/resources/sheets-cache/skilltree.serial`
* `vendor/ryzom_extra/resources/sheets-cache/visual_slot.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_de_creature.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_de_faction.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_de_item.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_de_outpost.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_de_outpost_building.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_de_outpost_squad.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_de_place.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_de_sbrick.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_de_skill.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_de_sphrase.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_de_title.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_de_uxt.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_en_creature.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_en_faction.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_en_item.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_en_outpost.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_en_outpost_building.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_en_outpost_squad.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_en_place.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_en_sbrick.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_en_skill.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_en_sphrase.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_en_title.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_en_uxt.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_es_creature.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_es_faction.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_es_item.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_es_outpost.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_es_outpost_building.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_es_outpost_squad.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_es_place.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_es_sbrick.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_es_skill.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_es_sphrase.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_es_title.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_es_uxt.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_fr_creature.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_fr_faction.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_fr_item.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_fr_outpost.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_fr_outpost_building.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_fr_outpost_squad.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_fr_place.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_fr_sbrick.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_fr_skill.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_fr_sphrase.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_fr_title.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_fr_uxt.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_ru_creature.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_ru_faction.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_ru_item.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_ru_outpost.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_ru_outpost_building.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_ru_outpost_squad.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_ru_place.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_ru_sbrick.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_ru_skill.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_ru_sphrase.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_ru_title.serial`
* `vendor/ryzom_extra/resources/sheets-cache/words_ru_uxt.serial`
* `vendor/ryzom_extra/ryzom_extra.php`
* `vendor/ryzom_extra/tests/`
* `vendor/ryzom_extra/tests/RyzomExtra/`
* `vendor/ryzom_extra/tests/RyzomExtra/AtysDateTest.php`
* `vendor/ryzom_extra/tests/RyzomExtra/CacheTest.php`
* `vendor/ryzom_extra/tests/RyzomExtra/GuildIconBuilderTest.php`
* `vendor/ryzom_extra/tests/RyzomExtra/RyzomClockTest.php`
* `vendor/ryzom_extra/tests/atysdate.php`
* `vendor/ryzom_extra/tests/buildings.php`
* `vendor/ryzom_extra/tests/dump-dataset.php`
* `vendor/ryzom_extra/tests/guild-icon.php`
* `vendor/ryzom_extra/tests/items.php`
* `vendor/ryzom_extra/tests/memory_usage.php`
* `vendor/ryzom_extra/tests/outpost.php`
* `vendor/ryzom_extra/tests/sbrick.php`
* `vendor/ryzom_extra/tests/sheetid.php`
* `vendor/ryzom_extra/tests/translation.php`
* `vendor/ryzom_extra/tests/visual_slot.php`

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:19.1.0

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app/src/main/AndroidManifest.xml
* assets/ => app/src/main/assets/
* res/ => app/src/main/res/
* src/ => app/src/main/java/

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
