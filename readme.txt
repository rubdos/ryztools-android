Welcome to Android Ryztools
===========================

Source of the Ryztools Android app.

Any help with further development is appreciated!


Code style
----------

General ADT setting, with the exceptions of:
* Indentation is one tab for all files.
* Line lengths are 120 characters (tab is four characters).


Setup
-----
Clone this repo somewhere.
go to the tools folder of this checkout, set the path to ryzom_extra in ryzomExtraToSQLite.php.
Run that php script to generate a sqlite3 database from the sheets.
Open eclipse, and choose File -> New -> Other.
Select Android project from existing source.
Done.


License
-------
http://opensource.org/licenses/GPL-3.0 GPL 3.0