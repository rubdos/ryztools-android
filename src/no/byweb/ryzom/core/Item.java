package no.byweb.ryzom.core;

public class Item {
	private String id;
	private String name;
	private String sheet;
	private String color;
	private String hpbuff;
	private String focusbuff;
	private String sapbuff;
	private String stabuff;
	private String stack;
	private String quality;
	private String locked;
	private String hp;
	private String locationName;
	private int locationPlace;
	private String sort;

	public Item(String id, String name, String sort, String sheet, String color, String stack, String quality,
			String locked, String hp, String locationName, int locationPlace) {
		super();
		this.id = id;
		this.name = name;
		this.sheet = sheet;
		this.color = color;
		this.stack = stack;
		this.quality = quality;
		this.locked = locked;
		this.hp = hp;
		this.locationName = locationName;
		this.locationPlace = locationPlace;
		this.sort = sort;
	}

	public Item(String id, String name, String sort, String sheet, String color, String hpbuff, String focusbuff,
			String sapbuff, String stabuff, String stack, String quality, String locked, String hp,
			String locationName, int locationPlace) {
		super();
		this.id = id;
		this.name = name;
		this.sheet = sheet;
		this.color = color;
		this.hpbuff = hpbuff;
		this.focusbuff = focusbuff;
		this.sapbuff = sapbuff;
		this.stabuff = stabuff;
		this.stack = stack;
		this.quality = quality;
		this.locked = locked;
		this.hp = hp;
		this.locationName = locationName;
		this.locationPlace = locationPlace;
		this.sort = sort;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSheet() {
		return sheet;
	}

	public String getColor() {
		return color;
	}

	public String getColorName() {
		try {
			int colorInt = Integer.parseInt(color);
			switch (colorInt) {
			case 0:
				return "Red";
			case 1:
				return "Beige";
			case 2:
				return "Green";
			case 3:
				return "Turquoise";
			case 4:
				return "Blue";
			case 5:
				return "Purple";
			case 6:
				return "White";
			case 7:
				return "Black";
			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return color;
	}
	
	public String getHpbuff() {
		return hpbuff;
	}
	
	public String getFocusbuff() {
		return focusbuff;
	}
	
	public String getSapbuff() {
		return sapbuff;
	}
	
	public String getStabuff() {
		return stabuff;
	}

	public String getStack() {
		return stack;
	}

	public String getQuality() {
		return quality;
	}

	public String getLocked() {
		return locked;
	}

	public String getHp() {
		return hp;
	}

	public String getLocationName() {
		return locationName;
	}

	public int getLocationPlace() {
		return locationPlace;
	}

	public String getSort() {
		return sort;
	}

}
