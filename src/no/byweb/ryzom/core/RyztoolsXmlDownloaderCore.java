package no.byweb.ryzom.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.util.Log;

/**
 * Downloader.
 * 
 * The downloader will take care of downloading data, and validates it. If file
 * is valid, it will store it.
 */
public class RyztoolsXmlDownloaderCore {
	static final String TAG = "RyztoolsXmlDownloaderCore";

	final Context context;
	String url;
	String params;

	public RyztoolsXmlDownloaderCore(final Context context) {
		this.context = context;
	}

	public void run(String url, String params) {
		this.url = url;
		this.params = params;
		String URL = RyztoolsApp.apiBasePath + url;
		if (this.params != null) {
			URL += this.params;
		}
		Log.d(TAG, URL);
		BufferedReader in = null;
		String data = null;

		try {
			HttpClient client = new DefaultHttpClient();
			URI webSite = new URI(URL);
			HttpGet request = new HttpGet();
			request.setURI(webSite);
			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer sb = new StringBuffer("");
			String l = "";
			String nl = System.getProperty("line.separator");
			while ((l = in.readLine()) != null) {
				sb.append(l + nl);
			}
			in.close();
			data = sb.toString();
		} catch (URISyntaxException e) {
			e.printStackTrace();
			data = null;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			data = null;
		} catch (IOException e) {
			e.printStackTrace();
			data = null;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		if (data != null) {
			Log.d(TAG, "Some data was downloaded.");
			boolean res = RyztoolsApp.validateXml(data, url);
			if (res != false) {
				Log.d(TAG, "The xml was accepted, continuing…");
				if (url.equals(RyztoolsApp.URL_TIME)) {
					FileHandler.saveFileInternally("time.xml", data.getBytes(), context);
				} else if ((url.equals(RyztoolsApp.URL_CHARACTER)) || (url.equals(RyztoolsApp.URL_GUILD))) {
					FileHandler.saveFileInternally(params + ".xml", data.getBytes(), context);
				}
			} else {
				Log.d(TAG, "Xml was not accepted, not saving.");
			}

		} else {
			Log.d(TAG, "Somehow the download failed.");
		}
	}

}
