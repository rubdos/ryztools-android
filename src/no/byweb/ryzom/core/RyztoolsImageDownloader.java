package no.byweb.ryzom.core;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

public class RyztoolsImageDownloader extends AsyncTask<String, Integer, String> {
	static final String TAG = "RyztoolsImageDownloader";

	private final String DIRECTORY = Environment.getExternalStorageDirectory() + "/ryztools_cache/";

	final String url;
	final String folderName;
	final String fileName;

	int outWidth = -1;
	int outHeight = -1;
	String outMimeType = null;
	byte[] bs;

	protected File file;

	boolean available = false;

	public RyztoolsImageDownloader(final String url, String folderName, String fileName) {
		this.url = url;
		this.folderName = folderName;
		this.fileName = fileName;
	}

	@Override
	protected String doInBackground(String... arg0) {
		File myDir = new File(DIRECTORY, folderName);
		myDir.mkdirs();

		file = new File(DIRECTORY + "/" + folderName, fileName);
		if (file.exists()) {
			available = true;
			Log.d(TAG, "Image file exists.");
			return null;
		}

		String URL = RyztoolsApp.apiBasePath + url;
		Log.d(TAG, URL);

		ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

		try {
			HttpClient client = new DefaultHttpClient();
			URI webSite;
			webSite = new URI(URL);
			HttpGet request = new HttpGet();
			request.setURI(webSite);
			HttpResponse response = client.execute(request);
			InputStream input = response.getEntity().getContent();

			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = input.read(buffer)) != -1) {
				byteBuffer.write(buffer, 0, len);
			}
			byteBuffer.flush();
			byteBuffer.close();

			bs = byteBuffer.toByteArray();

			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeByteArray(bs, 0, bs.length, options);

			input.close();

			outWidth = options.outWidth;
			outHeight = options.outHeight;
			outMimeType = options.outMimeType;

			if ((outWidth != -1) && (outHeight != -1)) {
				if ((options.outMimeType != null) && (options.outMimeType.equals("image/png"))) {
					if (saveFile() == true) {
						available = true;
						Log.d(TAG, "Image downloaded, and filename was: " + folderName + "/" + fileName);
					} else {
						Log.d(TAG, "Could not save image file: " + folderName + "/" + fileName);
					}
				} else {
					Log.d(TAG, "The downloaded image was not in a recognised format, aborting.");
				}
			} else {
				Log.d(TAG, "The downloaded image was not recognised as an image, aborting.");
			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	protected boolean getFile() {
		return available;
	}

	protected boolean saveFile() {
		boolean worked = false;

		FileOutputStream out = null;

		try {
			out = new FileOutputStream(file);
			out.write(bs);
			out.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
				worked = true;
			} catch (IOException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return worked;
	}

}
