package no.byweb.ryzom.pages;

import java.io.File;
import java.io.IOException;

import no.byweb.ryzom.character.CharactersActivity;
import no.byweb.ryzom.core.DbRyzomExtra;
import no.byweb.ryzom.core.DbRyztools;
import no.byweb.ryzom.core.RyztoolsApp;
import no.byweb.ryzom.dev.R;
import no.byweb.ryzom.guild.GuildsActivity;
import no.byweb.ryzom.preferences.PreferencesActivity;
import no.byweb.ryzom.seasonedmats.SeasonedMatsActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {
	static final String TAG = "MainActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		TextView tv = (TextView) findViewById(R.id.txtAppName);
		tv.setText("Ryztools v " + getVersion());

		DbRyzomExtra myDbHelper;
		myDbHelper = new DbRyzomExtra(this);

		// Clean up files that was cached in version 11 and 12.
		File directory = getDir("images", Context.MODE_PRIVATE);
		if (directory.exists()) {
			DeleteRecursive(directory);
		}

		try {
			myDbHelper.createDataBase();
		} catch (IOException e) {
			e.printStackTrace();
		}
		myDbHelper.close();
		
		DbRyztools myDbHelper2;
		myDbHelper2 = new DbRyztools(this);
		try {
			myDbHelper2.createDataBase();
		} catch (IOException e) {
			e.printStackTrace();
		}
		myDbHelper2.close();

	}

	private static void DeleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory()) {
			for (File child : fileOrDirectory.listFiles()) {
				DeleteRecursive(child);
			}
		}

		fileOrDirectory.delete();
	}

	@Override
	protected void onResume() {
		super.onResume();
		((RyztoolsApp) getApplication()).activityResumed();
	}

	@Override
	protected void onPause() {
		super.onPause();
		((RyztoolsApp) getApplication()).activityPaused();
	}

	public String getVersion() {
		String strVersion;

		PackageInfo packageInfo;

		try {
			packageInfo = getApplicationContext().getPackageManager().getPackageInfo(
					getApplicationContext().getPackageName(), 0);
			strVersion = packageInfo.versionName;
		} catch (NameNotFoundException e) {
			strVersion = "X";
		}

		return strVersion;
	}

	public void btnSeasonChange_onClick(View view) {
		Intent intent = new Intent(this, SeasonChangeActivity.class);
		startActivity(intent);
	}

	public void btnAbout_onClick(View view) {
		Intent intent = new Intent(this, AboutActivity.class);
		startActivity(intent);
	}

	public void btnMalusCrafting_onClick(View view) {
		Intent intent = new Intent(this, MalusCraftingActivity.class);
		startActivity(intent);
	}

	public void btnCharacters_onClick(View view) {
		Intent intent = new Intent(this, CharactersActivity.class);
		startActivity(intent);
	}

	public void btnGuilds_onClick(View view) {
		Intent intent = new Intent(this, GuildsActivity.class);
		startActivity(intent);
	}

	public void btnInventory_onClick(View view) {
		Intent intent = new Intent(this, InventoryActivity.class);
		startActivity(intent);
	}

	public void btnInventoryForaged_onClick(View view) {
		Intent intent = new Intent(this, InventoryForagedActivity.class);
		startActivity(intent);
	}

	public void btnPreferences_onClick(View view) {
		Intent intent = new Intent(this, PreferencesActivity.class);
		startActivity(intent);
	}
	
	public void btnSeasonedMats_onClick(View view) {
		Intent intent = new Intent(this, SeasonedMatsActivity.class);
		startActivity(intent);
	}

	public void btnExit_onClick(View view) {
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.exit:
			finish();
			return true;
		case R.id.settings:
			Intent prefsIntent = new Intent(this, PreferencesActivity.class);
			startActivity(prefsIntent);
			return true;
		default:
			return false;
		}
	}

}
