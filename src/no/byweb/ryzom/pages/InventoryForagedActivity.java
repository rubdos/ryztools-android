package no.byweb.ryzom.pages;

import java.util.ArrayList;
import java.util.List;

import no.byweb.ryzom.core.GetItems;
import no.byweb.ryzom.core.Item;
import no.byweb.ryzom.core.RyztoolsImageDownloader;
import no.byweb.ryzom.dev.R;
import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.support.v4.app.NavUtils;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class InventoryForagedActivity extends Activity implements OnClickListener {
	static final String TAG = "InventoryForagedActivity";

	TextView tv;
	PopulateList updater;

	private String sheetId = null;

	private String grade;
	private String ecosystem;
	private String subType;
	private String matType;
	private String textOut;

	private List<Item> items = new ArrayList<Item>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inventory_foraged);

		String[] SavedFiles = getApplicationContext().fileList();
		updater = new PopulateList(SavedFiles, this);
		updater.execute();
	}

	private class PopulateList extends GetItems {

		public PopulateList(String[] xmlFiles, Context context) {
			super(xmlFiles, context);
		}

		@Override
		protected void onPostExecute(String result) {
			populate(null);
		}

		public void populate(String filter) {
			Item itemObj;
			for (int i = 0; i < allItems.size(); i++) {
				Cursor cursor = null;
				itemObj = allItems.get(i);

				sheetId = itemObj.getSheet().substring(0, itemObj.getSheet().length() - 6);
				try {
					cursor = myDbHelper.getReadableDatabase().query("items",
							new String[] { "type", "icon_main", "ecosystem", "grade", "is_looted", "txt" },
							"_id = '" + sheetId + "'", null, null, null, null);
					if (cursor.moveToFirst()) {
						if (cursor.getString(0) != null) {
							if ((cursor.getInt(0) == 6) && (cursor.getInt(4) == 0)) {
								// Grade
								switch (cursor.getInt(3)) {
								case 20:
									grade = "Basic";
									break;
								case 35:
									grade = "Fine";
									break;
								case 50:
									grade = "Choice";
									break;
								case 65:
									grade = "Excellent";
									break;
								case 80:
									grade = "Supreme";
									break;
								default:
									grade = null;
									break;
								}

								// Ecosystem
								switch (cursor.getInt(2)) {
								case 0:
									ecosystem = "";
									break;
								case 1:
									ecosystem = "Desert";
									break;
								case 2:
									ecosystem = "Forest";
									break;
								case 3:
									ecosystem = "Lakes";
									break;
								case 4:
									ecosystem = "Jungle";
									break;
								case 6:
									ecosystem = "Pr";
									break;
								default:
									ecosystem = null;
									break;
								}

								if (grade != null) {
									// Sub type
									subType = Character.toUpperCase(cursor.getString(5).charAt(0))
											+ cursor.getString(5).substring(1);
									if (cursor.getString(1).equals("mp_shell.png")) {
										matType = "Shell";
									} else if (cursor.getString(1).equals("mp_wood_node.png")) {
										matType = "Node";
									} else if (cursor.getString(1).equals("mp_bark.png")) {
										matType = "Bark";
									} else if (cursor.getString(1).equals("mp_wood.png")) {
										matType = "Wood";
									} else if (cursor.getString(1).equals("mp_resin.png")) {
										matType = "Resin";
									} else if (cursor.getString(1).equals("mp_oil.png")) {
										matType = "Oil";
									} else if (cursor.getString(1).equals("mp_sap.png")) {
										matType = "Sap";
									} else if (cursor.getString(1).equals("mp_seed.png")) {
										matType = "Seed";
									} else if (cursor.getString(1).equals("mp_fiber.png")) {
										matType = "Fiber";
									} else if (cursor.getString(1).equals("mp_amber.png")) {
										matType = "Amber";
									} else {
										matType = null;
									}

									if (matType != null) {
										int resID = getResources().getIdentifier(
												"txt" + matType + subType + grade + ecosystem, "id", getPackageName());
										if (resID != 0) {
											TextView tv = (TextView) findViewById(resID);
											textOut = itemObj.getStack();
											if (!tv.getText().equals("")) {
												textOut = String.valueOf(Integer.parseInt(textOut)
														+ Integer.parseInt(tv.getText().toString()));
											} else {
												tv.setOnClickListener(InventoryForagedActivity.this);
												tv.setTag(sheetId);
											}
											tv.setText(textOut);
											items.add(itemObj);
										}
									}

								}
							}
						}
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					if (cursor != null) {
						cursor.close();
					}
				}
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return false;
	}

	@Override
	public void onClick(View view) {
		String itemObj = (String) view.getTag();

		Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.dialog_locate_items);
		dialog.setTitle("Locations");

		dialog.show();

		TextView tv1 = (TextView) dialog.findViewById(R.id.txtItemName);
		TextView tv2 = (TextView) dialog.findViewById(R.id.txtItemLocations);

		String text = "";
		for (Item item : items) {
			if (item.getSheet().equals(itemObj + ".sitem")) {
				tv1.setText(item.getName());

				text += "<p>" + item.getStack() + "x q" + item.getQuality() + " in " + item.getLocationName();
				switch (item.getLocationPlace()) {
				case GetItems.LOC_INVENTORY:
					text += " Bag";
					break;
				case GetItems.LOC_ROOM:
					text += " Room";
					break;
				case GetItems.LOC_PET1:
					text += " Pet 1";
					break;
				case GetItems.LOC_PET2:
					text += " Pet 2";
					break;
				case GetItems.LOC_PET3:
					text += " Pet 3";
					break;
				case GetItems.LOC_PET4:
					text += " Pet 4";
					break;
				default:
					text += " Unknown location";
					break;
				}
				text += "</p>";
			}
		}
		tv2.setText(Html.fromHtml(text));

		String itemIcon = itemObj + ".sitem";
		ImageDownloader imageDownloader = new ImageDownloader("item_icon.php?sheetid=" + itemIcon, "item",
				itemIcon.replace("&", "_"), (ImageView) dialog.findViewById(R.id.imgItemIcon));
		imageDownloader.execute();
	}

	private class ImageDownloader extends RyztoolsImageDownloader {

		private ImageView iv;

		public ImageDownloader(String url, String folderName, String fileName, ImageView iv) {
			super(url, folderName, fileName);
			this.iv = iv;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			final float scale = getResources().getDisplayMetrics().density;
			int image = (int) (40 * scale + 0.5f);

			Bitmap bitmap;

			if (getFile() == true) {
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inPreferredConfig = Bitmap.Config.ARGB_8888;
				Bitmap b = BitmapFactory.decodeFile(file.toString(), options);
				bitmap = Bitmap.createScaledBitmap(b, image, image, false);
			} else {
				bitmap = BitmapFactory.decodeResource(getResources(), R.raw.icon_unknown);
			}

			Bitmap bmOverlay = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_4444);
			Canvas canvas = new Canvas(bmOverlay);
			canvas.drawBitmap(bitmap, 0, 0, null);

			iv.setImageBitmap(bmOverlay);
		}

	}
}
