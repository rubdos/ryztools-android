package no.byweb.ryzom.character;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.text.NumberFormat;

import no.byweb.ryzom.core.DbRyzomExtra;
import no.byweb.ryzom.dev.R;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class CharacterActivity extends Activity {
	static final String TAG = "CharacterActivity";

	String XML_FILE;

	double maxSkill = 0;
	double constitution = 0;
	double strength = 0;
	double intelligence = 0;
	double balance = 0;
	long played = 0;
	String dappers = null;

	String charName = null;
	String gender = null;
	String titleid = null;

	boolean worked = false;

	TextView textview;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_character);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {

			XML_FILE = extras.getString("XML_FILE");
			getData();
			populateEquipmentLevels();
			if (worked) {
				if (charName != null) {
					textview = (TextView) findViewById(R.id.txtCharName);
					textview.setText(charName);
				}
				if (titleid != null) {
					textview = (TextView) findViewById(R.id.txtCharTitle);

					String title = titleid;

					DbRyzomExtra myDbHelper;
					myDbHelper = new DbRyzomExtra(this);
					try {
						myDbHelper.openDataBase();
						Cursor result = myDbHelper.getReadableDatabase().query("words_en_title",
								new String[] { "name", "women_name" }, "_id = '" + titleid.toLowerCase() + "'", null,
								null, null, null);
						if (result.moveToFirst()) {
							if (result.getString(0) != null) {
								title = result.getString(0);
								if ((gender.equals("f")) && (result.getString(1) != null)) {
									title = result.getString(1);
								}
							}
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
					myDbHelper.close();

					textview.setText(title);
				}

				if (played > 0) {
					textview = (TextView) findViewById(R.id.txtPlayed);
					int d = (int) (played / 86400);
					int h = (int) (played % 86400 / 3600);
					int m = (int) (played % 3600 / 60);
					int s = (int) (played % 3600 % 60);
					String playedString = String.valueOf(d) + " day" + (d != 1 ? "s" : "") + " " + String.valueOf(h)
							+ " hour" + (h != 1 ? "s" : "") + " " + String.valueOf(m) + " minute" + (m != 1 ? "s" : "")
							+ " " + String.valueOf(s) + " second" + (s != 1 ? "s" : "");
					textview.setText("Played: " + playedString);
				}

				if (dappers != null) {
					textview = (TextView) findViewById(R.id.txtDappers);
					try {
						textview.setText("Dappers: " + NumberFormat.getInstance().format(Long.parseLong(dappers)));
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			finish();
		}
	}

	public void btnInventory_onClick(View view) {
		Intent intent = new Intent(this, CharacterInventoryActivity.class);
		intent.putExtra("XML_FILE", XML_FILE);
		startActivity(intent);
	}

	public void btnSkills_onClick(View view) {
		Intent intent = new Intent(this, CharacterSkillsActivity.class);
		intent.putExtra("XML_FILE", XML_FILE);
		startActivity(intent);
	}

	private void getData() {
		FileInputStream fis = null;
		String fileContent = null;
		XmlPullParserFactory factory;

		try {
			fis = getApplicationContext().openFileInput(XML_FILE);
			byte[] buffer = new byte[fis.available()];
			while (fis.read(buffer) != -1) {
				fileContent = new String(buffer);
			}
			factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();
			xpp.setInput(new StringReader(fileContent));
			int eventType = xpp.getEventType();
			String tag = null;
			boolean inSkills = false;
			boolean inCharacteristics = false;
			while (eventType != XmlPullParser.END_DOCUMENT) {
				String tagName = xpp.getName();
				switch (eventType) {
				case XmlPullParser.START_TAG:
					tag = tagName;
					if ((xpp.getDepth() == 3) && tag.equals("skills")) {
						inSkills = true;
					} else if ((xpp.getDepth() == 3) && tag.equals("characteristics")) {
						inCharacteristics = true;
					}
					break;
				case XmlPullParser.TEXT:
					if ((xpp.getDepth() == 4) && inSkills) {
						try {
							if (maxSkill < Double.parseDouble(xpp.getText())) {
								maxSkill = Double.parseDouble(xpp.getText());
							}
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
					} else if ((xpp.getDepth() == 4) && inCharacteristics) {
						try {
							if (tag.equals("constitution")) {
								constitution = Double.parseDouble(xpp.getText());
							} else if (tag.equals("strength")) {
								strength = Double.parseDouble(xpp.getText());
							} else if (tag.equals("intelligence")) {
								intelligence = Double.parseDouble(xpp.getText());
							} else if (tag.equals("wellbalanced")) {
								balance = Double.parseDouble(xpp.getText());
							}
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
					} else if (xpp.getDepth() == 3) {
						try {
							if (tag.equals("name")) {
								charName = xpp.getText();
							} else if (tag.equals("played")) {
								played = Long.parseLong(xpp.getText());
							} else if (tag.equals("gender")) {
								gender = xpp.getText();
								if (xpp.getText().equals("m")) {
									gender = "m";
								} else if (xpp.getText().equals("f")) {
									gender = "f";
								}
							} else if (tag.equals("titleid")) {
								titleid = xpp.getText();
							} else if (tag.equals("money")) {
								dappers = xpp.getText();
							}
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
					}
					break;
				case XmlPullParser.END_TAG:
					tag = tagName;
					if ((xpp.getDepth() == 3) && tag.equals("skills")) {
						inSkills = false;
					} else if ((xpp.getDepth() == 3) && tag.equals("characteristics")) {
						inCharacteristics = false;
					}
					break;
				default:
					break;
				}
				eventType = xpp.next();
			}
			worked = true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}
	}

	private void populateEquipmentLevels() {
		int tmp;
		int tmp_comp;

		if (worked) {
			if (maxSkill > 0) {
				textview = (TextView) findViewById(R.id.txtLightArmor);
				tmp = (int) (maxSkill + 25);
				if (tmp > 250) {
					tmp = 250;
				}
				textview.setText("Light armor: " + String.valueOf(tmp));

				textview = (TextView) findViewById(R.id.txtJewels);
				textview.setText("Jewels: " + String.valueOf(tmp));
			}

			if ((constitution > 0) && (maxSkill > 0)) {
				textview = (TextView) findViewById(R.id.txtMediumArmor);
				tmp = (int) (constitution * 1.5);
				if (tmp > 250) {
					tmp = 250;
				}
				tmp_comp = (int) (maxSkill + 25);
				if (tmp_comp > 250) {
					tmp_comp = 250;
				}
				tmp = Math.min(tmp, tmp_comp);
				textview.setText("Medium armor: " + String.valueOf(tmp));
			}

			if (constitution > 0) {
				textview = (TextView) findViewById(R.id.txtHeavyArmor);
				tmp = (int) (constitution + 10);
				if (tmp > 250) {
					tmp = 250;
				}
				textview.setText("Heavy armor: " + String.valueOf(tmp));

				textview = (TextView) findViewById(R.id.txtShields);
				textview.setText("Shields: " + String.valueOf(tmp));
			}

			if (constitution > 0) {
				textview = (TextView) findViewById(R.id.txtBucklers);
				tmp = (int) (constitution * 1.5);
				if (tmp > 250) {
					tmp = 250;
				}
				tmp_comp = (int) (constitution + 10);
				if (tmp_comp > 250) {
					tmp_comp = 250;
				}
				tmp = Math.min(tmp, tmp_comp);
				textview.setText("Bucklers: " + String.valueOf(tmp));
			}

			if (strength > 0) {
				textview = (TextView) findViewById(R.id.txtMeleeWeapons);
				tmp = (int) (strength + 10);
				if (tmp > 250) {
					tmp = 250;
				}
				textview.setText("Melee Weapons: " + String.valueOf(tmp));
			}

			if (intelligence > 0) {
				textview = (TextView) findViewById(R.id.txtMagicAmplifiers);
				tmp = (int) (intelligence + 10);
				if (tmp > 250) {
					tmp = 250;
				}
				textview.setText("Magic Amplifiers: " + String.valueOf(tmp));
			}

			if (balance > 0) {
				textview = (TextView) findViewById(R.id.txtRangedWeaopns);
				tmp = (int) (balance + 10);
				if (tmp > 250) {
					tmp = 250;
				}
				textview.setText("Ranged Weaopns: " + String.valueOf(tmp));
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return false;
	}
}
