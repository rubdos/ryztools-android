package no.byweb.ryzom.seasonedmats;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import no.byweb.ryzom.dev.R;
import no.byweb.ryzom.seasonedmats.SeasonedMatsContent.SeasonedMatsItem;
import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MaterialListFragment extends ListFragment {
	static final String TAG = "MaterialListFragment";

	private static final String STATE_ACTIVATED_POSITION = "activated_position";

	private Callbacks mCallbacks = sDummyCallbacks;

	private int mActivatedPosition = ListView.INVALID_POSITION;

	public interface Callbacks {
		public void onItemSelected(String id);
	}

	private static Callbacks sDummyCallbacks = new Callbacks() {
		@Override
		public void onItemSelected(String id) {
		}
	};

	public MaterialListFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ArrayAdapter<SeasonedMatsItem> adapter = null;
		adapter = new MyListAdapter(SeasonedMatsContent.ITEMS);
		setListAdapter(adapter);
	}

	private class MyListAdapter extends ArrayAdapter<SeasonedMatsContent.SeasonedMatsItem> {

		private List<SeasonedMatsContent.SeasonedMatsItem> materials;

		public MyListAdapter(List<SeasonedMatsItem> materials) {
			super(getActivity().getApplicationContext(), R.layout.list_item_item_nopadding, materials);
			this.materials = materials;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// Make sure we have a view to work with (may have been given a null)
			View itemView = convertView;
			if (itemView == null) {
				itemView = getActivity().getLayoutInflater().inflate(R.layout.list_item_item_nopadding, parent, false);
			}

			SeasonedMatsContent.SeasonedMatsItem currentKey = materials.get(position);

			itemView.setTag(currentKey.getId());

			TextView nameText = (TextView) itemView.findViewById(R.id.item_textName);

			String message = "";
			if (currentKey.getName() != null) {
				message += currentKey.getName();
			}
			nameText.setText(message);

			ImageView imageView = (ImageView) itemView.findViewById(R.id.item_imgIcon);

			try {
				InputStream ims = getActivity().getAssets().open("icon_images/" + currentKey.getIcon());
				Drawable d = Drawable.createFromStream(ims, null);
				imageView.setImageDrawable(d);
			} catch (IOException e) {
				e.printStackTrace();
			}

			return itemView;
		}

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		if (savedInstanceState != null && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
			setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException("Activity must implement fragment's callbacks.");
		}

		mCallbacks = (Callbacks) activity;
	}

	@Override
	public void onDetach() {
		super.onDetach();

		mCallbacks = sDummyCallbacks;
	}

	@Override
	public void onListItemClick(ListView listView, View view, int position, long id) {
		super.onListItemClick(listView, view, position, id);

		mCallbacks.onItemSelected(SeasonedMatsContent.ITEMS.get(position).getId());
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (mActivatedPosition != ListView.INVALID_POSITION) {
			outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
		}
	}

	public void setActivateOnItemClick(boolean activateOnItemClick) {
		getListView().setChoiceMode(activateOnItemClick ? ListView.CHOICE_MODE_SINGLE : ListView.CHOICE_MODE_NONE);
	}

	private void setActivatedPosition(int position) {
		if (position == ListView.INVALID_POSITION) {
			getListView().setItemChecked(mActivatedPosition, false);
		} else {
			getListView().setItemChecked(position, true);
		}

		mActivatedPosition = position;
	}
}
