package no.byweb.ryzom.guild;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import no.byweb.ryzom.dev.R;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class GuildMembersActivity extends Activity {
	static final String TAG = "GuildMembersActivity";

	String XML_FILE;
	private List<Member> allMembers = new ArrayList<Member>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_guild_members);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			XML_FILE = extras.getString("XML_FILE");

			GetMembers members = new GetMembers();
			members.execute();
		} else {
			finish();
		}
	}

	public class GetMembers extends AsyncTask<String, Integer, String> {
		private XmlPullParserFactory factory;
		private FileInputStream fis = null;
		private String fileContent = null;

		private boolean inMembers = false;
		private boolean inMember = false;

		private String name;
		private String grade;
		private String joined;

		@Override
		protected String doInBackground(String... arg0) {

			try {
				fis = openFileInput(XML_FILE);
				byte[] buffer = new byte[fis.available()];
				while (fis.read(buffer) != -1) {
					fileContent = new String(buffer);
				}
				factory = XmlPullParserFactory.newInstance();
				factory.setNamespaceAware(true);
				XmlPullParser xpp = factory.newPullParser();
				xpp.setInput(new StringReader(fileContent));
				int eventType = xpp.getEventType();
				String tag = null;

				while (eventType != XmlPullParser.END_DOCUMENT) {
					String tagName = xpp.getName();
					switch (eventType) {
					case XmlPullParser.START_TAG:
						tag = tagName;
						if (xpp.getDepth() == 3) {
							if (tag.equals("members")) {
								inMembers = true;
							}
						} else if ((inMembers == true) && (xpp.getDepth() == 4)) {
							if (tag.equals("member")) {
								inMember = true;
							}
						}
						break;
					case XmlPullParser.TEXT:
						if (inMember == true) {
							if (tag.equals("name")) {
								name = xpp.getText();
							} else if (tag.equals("grade")) {
								if (xpp.getText().equals("Leader")) {
									grade = "1";
								} else if (xpp.getText().equals("HighOfficer")) {
									grade = "2";
								} else if (xpp.getText().equals("Officer")) {
									grade = "3";
								} else {
									grade = "4";
								}
							} else if (tag.equals("joined")) {
								joined = xpp.getText();
							}
						}
						break;
					case XmlPullParser.END_TAG:
						tag = tagName;
						if (xpp.getDepth() == 3) {
							inMembers = false;
						} else if (xpp.getDepth() == 4) {
							if (inMember == true) {
								allMembers.add(new Member(name, grade, joined));
								inMember = false;
							}
						}
						break;
					default:
						break;
					}
					eventType = xpp.next();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			populateListView(R.id.guildMembers, allMembers, R.id.guildMembersEmpty);
		}
	}

	private void populateListView(int id, List<Member> items, int emptyId) {
		ArrayAdapter<Member> adapter = null;
		try {
			adapter = new MyListAdapter(this, items);

			adapter.notifyDataSetChanged();
			ListView list = (ListView) findViewById(id);
			list.setEmptyView(findViewById(emptyId));

			list.setAdapter(adapter);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class MyListAdapter extends ArrayAdapter<Member> {

		private List<Member> items;

		public MyListAdapter(Context context, List<Member> items) {
			super(context, R.layout.list_item_preferences_api_key, items);
			this.items = items;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// Make sure we have a view to work with (may have been given a null)
			View itemView = convertView;
			if (itemView == null) {
				itemView = getLayoutInflater().inflate(R.layout.list_item_default, parent, false);
			}

			Collections.sort(items, new CustomComparator());
			Member currentKey = items.get(position);

			TextView nameText = (TextView) itemView.findViewById(R.id.item_textName);

			String message = "";
			if (currentKey.getName() != null) {
				message += currentKey.getName();
			} else {
				message += "Unknown";
			}
			if (currentKey.getGrade() != null) {
				if (currentKey.getGrade().equals("4")) {
					message += " (Member)";
				} else if (currentKey.getGrade().equals("3")) {
					message += " (Officer)";
				} else if (currentKey.getGrade().equals("2")) {
					message += " (High officer)";
				} else if (currentKey.getGrade().equals("1")) {
					message += " (Leader)";
				}
			} else {
				message += " (Unknown)";
			}

			nameText.setText(message);

			return itemView;
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return false;
	}

	private class CustomComparator implements Comparator<Member> {
		@Override
		public int compare(Member o1, Member o2) {
			String comperator = o1.getGrade() + o1.getName();
			return comperator.compareTo(o2.getGrade() + o2.getName());
		}
	}

	class Member {
		private String name;
		private String grade;
		private String joined;

		public Member(String name, String grade, String joined) {
			super();
			this.name = name;
			this.grade = grade;
			this.joined = joined;
		}

		public String getName() {
			return name;
		}

		public String getGrade() {
			return grade;
		}

		public String getJoined() {
			return joined;
		}

	}

}
