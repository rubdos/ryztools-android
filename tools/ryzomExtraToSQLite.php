#!/usr/bin/php5
<?php
/*
 * How to use:
 * Checkout this git repository: https://github.com/nimetu/ryzom_extra
 * Change the $sheets variable to point to where that checkout was.
 * Run this script.
 */
$sheets = '/path/to/ryzom_extra/resources/sheet-cache/'; // Has to end with a trailing slash.


$db = new SQLite3('../assets/ryzom_extra.sqlite3');

if (1==1) {
	$table = 'resource_stats';
	$table = 'craftplan';
}

if (1==1) {
	$table = 'words_en_faction';

	$items = unserialize(file_get_contents($sheets . $table . '.serial'));
	echo "\nPopulating {$table} (" . count($items) . " rows)\n";

	$db->exec("DROP TABLE IF EXISTS `{$table}`;");
	$db->exec("CREATE TABLE `{$table}` (
		`_id` TEXT PRIMARY KEY,
		`name` TEXT,
		`member` TEXT
	);");
	foreach ($items as $key => $item) {
		echo ".";
		$query = sprintf("INSERT INTO `{$table}` (`_id`, `name`, `member`) VALUES ('%s', %s, %s);",
			$db->escapeString($key),
			(((isset($item['name'])) && ($item['name'] !== '')) ? "'" . $db->escapeString($item['name']) . "'" : 'null'),
			(((isset($item['member'])) && ($item['member'] !== '')) ? "'" . $db->escapeString($item['member']) . "'" : 'null')
		);
		// echo $query . "\n";
		$db->exec($query);
	}
	echo "\n";
}

if (1==1) {
	$tables = array('words_en_item', 'words_en_skill');
	foreach ($tables as $table) {
		$items = unserialize(file_get_contents($sheets . $table . '.serial'));
		echo "\nPopulating {$table} (" . count($items) . " rows)\n";

		$db->exec("DROP TABLE IF EXISTS `{$table}`;");
		$db->exec("CREATE TABLE `{$table}` (
			`_id` TEXT PRIMARY KEY,
			`name` TEXT,
			`p` TEXT,
			`description` TEXT
		);");
		foreach ($items as $key => $item) {
			echo ".";
			$query = sprintf("INSERT INTO `{$table}` (`_id`, `name`, `p`, `description`) VALUES ('%s', %s, %s, %s);",
				$db->escapeString($key),
				(((isset($item['name'])) && ($item['name'] !== '')) ? "'" . $db->escapeString($item['name']) . "'" : 'null'),
				(((isset($item['p'])) && ($item['p'] !== '')) ? "'" . $db->escapeString($item['p']) . "'" : 'null'),
				(((isset($item['description'])) && ($item['description'] !== '')) ? "'" . $db->escapeString($item['description']) . "'" : 'null')
			);
			// echo $query . "\n";
			$db->exec($query);
		}
		echo "\n";
	}
}

if (1==1) {
	$table = 'words_en_title';

	$items = unserialize(file_get_contents($sheets . $table . '.serial'));
	echo "\nPopulating {$table} (" . count($items) . " rows)\n";

	$db->exec("DROP TABLE IF EXISTS `{$table}`;");
	$db->exec("CREATE TABLE `{$table}` (
		`_id` TEXT PRIMARY KEY,
		`name` TEXT,
		`women_name` TEXT
	);");
	foreach ($items as $key => $item) {
		echo ".";
		$query = sprintf("INSERT INTO `{$table}` (`_id`, `name`, `women_name`) VALUES ('%s', %s, %s);",
			$db->escapeString($key),
			(((isset($item['name'])) && ($item['name'] !== '')) ? "'" . $db->escapeString($item['name']) . "'" : 'null'),
			(((isset($item['women_name'])) && ($item['women_name'] !== '')) ? "'" . $db->escapeString($item['women_name']) . "'" : 'null')
		);
		// echo $query . "\n";
		$db->exec($query);
	}
	echo "\n";
}

if (1==1) {
	$table = 'items';

	$items = unserialize(file_get_contents($sheets . $table . '.serial'));
	echo "\nPopulating {$table} (" . count($items) . " rows)\n";

	$db->exec("DROP TABLE IF EXISTS `{$table}`;");
	$db->exec("CREATE TABLE `{$table}` (
		`_id` TEXT PRIMARY KEY,
		`type` INTEGER,
		`item_type` INTEGER,
		`race` INTEGER,
		`quality` INTEGER,
		`bulk` REAL,
		`icon_main` TEXT,
		`icon_back` TEXT,
		`icon_over` TEXT,
		`icon_over2` TEXT,
		`craftplan` TEXT,
		`skill` TEXT,
		`damage` INTEGER,
		`reach` INTEGER,
		`ecosystem` INTEGER,
		`grade` INTEGER,
		`mpft` INTEGER,
		`color` INTEGER,
		`is_looted` INTEGER,
		`is_mission` INTEGER,
		`index` INTEGER,
		`txt` TEXT,
		`icon_color_over` INTEGER,
		`icon_color_main` INTEGER,
		`icon_color_back` INTEGER
	);");

	$keys = array();
	$iconFields = array();

	foreach ($items as $item) {
		echo ".";
		foreach ($item as $key => $value) {
			$keys[$key] = $value;
		}
		$query = sprintf("INSERT INTO `items`
			(
				`_id`, `type`, `item_type`, `race`, `quality`, `bulk`, `icon_main`, `icon_back`, `icon_over`, `icon_over2`,
				`craftplan`, `skill`, `damage`, `reach`, `ecosystem`, `grade`, `mpft`, `color`, `is_looted`, `is_mission`,
				`index`, `txt`, `icon_color_over`, `icon_color_main`, `icon_color_back`
			)
			VALUES
			(
				'%s', %d, %d, %d, %d, %s, %s, %s, %s, %s,
				%s, %s, %d, %d, %d, %d, %d, %d, %d, %d,
				%d, %s, %d, %d, %d
			)
			;",
			$db->escapeString($item['sheetid']),
			(((isset($item['type'])) && (is_int($item['type']))) ? $item['type'] : 'null'),
			(((isset($item['item_type'])) && (is_int($item['item_type']))) ? $item['item_type'] : 'null'),
			(((isset($item['race'])) && (is_int($item['race']))) ? $item['race'] : 'null'),
			(((isset($item['quality'])) && (is_int($item['quality']))) ? $item['quality'] : 'null'),
			(((isset($item['bulk'])) && (is_float($item['bulk']))) ? $item['bulk'] : 'null'),
			(((isset($item['icon']['main'])) && ($item['icon']['main'] !== '')) ? "'" . $db->escapeString($item['icon']['main']) . "'" : 'null'),
			(((isset($item['icon']['back'])) && ($item['icon']['back'] !== '')) ? "'" . $db->escapeString($item['icon']['back']) . "'" : 'null'),
			(((isset($item['icon']['over'])) && ($item['icon']['over'] !== '')) ? "'" . $db->escapeString($item['icon']['over']) . "'" : 'null'),
			(((isset($item['icon']['over2'])) && ($item['icon']['over2'] !== '')) ? "'" . $db->escapeString($item['icon']['over2']) . "'" : 'null'),

			(((isset($item['craftplan'])) && ($item['craftplan'] !== '')) ? "'" . $db->escapeString($item['craftplan']) . "'" : 'null'),
			(((isset($item['skill'])) && ($item['skill'] !== '')) ? "'" . $db->escapeString($item['skill']) . "'" : 'null'),
			(((isset($item['damage'])) && (is_int($item['damage']))) ? $item['damage'] : 'null'),
			(((isset($item['reach'])) && (is_int($item['reach']))) ? $item['reach'] : 'null'),
			(((isset($item['ecosystem'])) && (is_int($item['ecosystem']))) ? $item['ecosystem'] : 'null'),
			(((isset($item['grade'])) && (is_int($item['grade']))) ? $item['grade'] : 'null'),
			(((isset($item['mpft'])) && (ctype_digit($item['mpft']))) ? $item['mpft'] : 'null'),
			(((isset($item['color'])) && (is_int($item['color']))) ? $item['color'] : 'null'),
			(((isset($item['is_looted'])) && (is_int($item['is_looted']))) ? $item['is_looted'] : 'null'),
			(((isset($item['is_mission'])) && (is_int($item['is_mission']))) ? $item['is_mission'] : 'null'),

			(((isset($item['index'])) && (is_int($item['index']))) ? $item['index'] : 'null'),
			(((isset($item['txt'])) && ($item['txt'] !== '')) ? "'" . $db->escapeString($item['txt']) . "'" : 'null'),
			(((isset($item['icon_color']['over'])) && (is_int($item['icon_color']['over']))) ? $item['icon_color']['over'] : 'null'),
			(((isset($item['icon_color']['main'])) && (is_int($item['icon_color']['main']))) ? $item['icon_color']['main'] : 'null'),
			(((isset($item['icon_color']['back'])) && (is_int($item['icon_color']['back']))) ? $item['icon_color']['back'] : 'null')
		);

		// echo $query . "\n";
		// die();
		$db->exec($query);
	}
	echo "\n\n";

	echo 'Keys found in the items.serial: ' . implode(', ', array_keys($keys)) . "\n";

	echo "\n";
	// var_dump($keys);
}
